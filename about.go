package main

import (

	//"github.com/davecgh/go-spew/spew"

	"net/http"
)

func about(w http.ResponseWriter, r *http.Request) {

	err := tpl.ExecuteTemplate(w, "about.html", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

	}

}

type person struct {
	FirstName  string
	LastName   string
	Subscribed bool
}

func form(w http.ResponseWriter, r *http.Request) {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "bill2-zendrulat.c9users.io")
	w.Header().Set("Access-Control-Allow-Origin", "https://preview.c9users.io")

	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	//r.ParseForm()

	//fmt.Println("this is r.form is ", r.Form)

	//metas := r.FormValue("Contact[]")
	//links := r.FormValue("link")
	//fmt.Println("this is the metas ", metas, "this is links ", links)

	switch r.Method {
	case http.MethodGet:

		err := tpl.ExecuteTemplate(w, "form.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

		return
	case http.MethodPost:
		// Handle POST requestsbody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		err := tpl.ExecuteTemplate(w, "form.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

	}

}
